//
//  RegistrationViewController.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 22.09.17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire


class EnterNumberVC: UIViewController, UITextFieldDelegate {
    
    

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var registrationStackView: UIStackView!
    @IBOutlet weak var phoneNumberLabel: UITextField!
    @IBOutlet var chooseCountry:DropMenuButton!
    
    

    
    let countries = try! Realm().objects(Countries.self)
    let dropDownMenu = DropMenuButton()
    var params : [String: String] = [:]
    var authCode = 0
    var count = -1
    enum options : Int {
        case login
        case registration
    }
    var kbHeight: CGFloat!
    
    var userOption = options.registration

    
    private lazy var backgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10.0
        return view
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        if userOption == options.login {
            self.title = "Login"
        }
        
        self.phoneNumberLabel.delegate = self
        
        let phoneNumberLine = UIView(frame: CGRect(x: 0, y: phoneNumberLabel.bounds.size.height, width: phoneNumberLabel.frame.size.width, height: 2))
        phoneNumberLine.backgroundColor = UIColor.lightGray
        phoneNumberLabel.addSubview(phoneNumberLine)
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "background.jpg")
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
        
        stackBackground(backgroundView, to: registrationStackView)

        chooseCountry.initMenu()
        
        NotificationCenter.default.addObserver(self, selector: #selector(EnterNumberVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EnterNumberVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }
    
//    func keyboardWillShow(notification: NSNotification) {
//        if let userInfo = notification.userInfo {
//            if let keyboardSize =  (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//                kbHeight = keyboardSize.height
//                self.animateTextField(up: true)
//            }
//        }
//    }
//
//    func keyboardWillHide(notification: NSNotification) {
//        self.animateTextField(up: false)
//    }
//
//    func animateTextField(up: Bool) {
//        var movement = (up ? -kbHeight : kbHeight)
    
//        UIView.animate(withDuration: 0.3, animations: {
//            self.view.frame = CGRectOffset(self.view.frame, 0, movement!)
//        })
//    }
    
    // Set white background for stackView
    private func stackBackground(_ view: UIView, to stackView: UIStackView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        stackView.insertSubview(view, at: 0)
        view.pin(to: stackView)
    }
    
    
    // We can input only digits in phone number text field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    
    @IBAction func nextButton(_ sender: UIButton) {
        let phoneCode = chooseCountry.phoneCode
        let digitsPhoneNumber = chooseCountry.digitsPhoneNumber
        if (phoneNumberLabel.text?.isEmpty)! || (phoneNumberLabel.text!.characters.count != digitsPhoneNumber) {
            errorMessage(errorDescription: "Phone number must have \(digitsPhoneNumber) digits")
        } else {
            activityIndicator.startAnimating()
            activityIndicator.isHidden = false
            params = [
                "phone_id": UIDevice.current.identifierForVendor!.uuidString,
                "phone_code": "\(phoneCode)",
                "phone_number": phoneNumberLabel.text!,
                "user_role" : "3"
            ]
            
            PostAndGetCode.getCode(phoneNumber: phoneNumberLabel.text!, phoneCode: phoneCode, success: { (response) -> Void in
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                let storyboard = UIStoryboard(name: "Confirm", bundle: nil)
                let confirmController = storyboard.instantiateViewController(withIdentifier :"ConfirmRegistration") as! ConfirmVC
                self.navigationController?.pushViewController(confirmController, animated: true)
                confirmController.paramsDictionary = self.params

            }) { (error) -> Void in
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                self.errorMessage(errorDescription: error)
            }
        }
    }
    
    
    func errorMessage(errorDescription: String) {
        let alert = UIAlertController(title: "Error", message: errorDescription, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
}



// White background for stack view
extension UIView {
    public func pin(to view: UIView) {
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: view.leadingAnchor),
            trailingAnchor.constraint(equalTo: view.trailingAnchor),
            topAnchor.constraint(equalTo: view.topAnchor),
            bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
}
