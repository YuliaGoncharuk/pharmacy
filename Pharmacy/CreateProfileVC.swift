//
//  CreateProfileViewController.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 25.09.17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import RealmSwift

class CreateProfileVC: UIViewController, SetAddress, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func newAddress(text: String?, longitude: String?, latitude: String?) {
        physicalAddress.text = text
        long = longitude!
        lat = latitude!
    }
    
    
    let picker = UIImagePickerController()
    let countries = try! Realm().objects(Countries.self)
    let paramsOfUser = try! Realm().objects(ParametresOfUser.self)
    var url = ""
    var flag = UIImage()
    var valueSentFromSecondViewController:String?
    var valueToDisplay = ""
    var button : UIButton?
    var long = ""
    var lat = ""
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var pharmacyAvatar: UIImageView!
    @IBOutlet var phamcacyName: UITextField!
    @IBOutlet var physicalAddress: UITextField!
    @IBOutlet weak var preAdminName: PreNamesList!
    @IBOutlet var adminName: UITextField!
    @IBOutlet var email: UITextField!
    @IBOutlet var phoneCode: UITextField!
    @IBOutlet var phone: UITextField!
    @IBOutlet var ID: UIButton!
    @IBOutlet var practiceCertificate: UIButton!
    @IBOutlet var pharmacyLicense: UIButton!
    @IBOutlet var besinessPermit: UIButton!
    @IBOutlet var pinCertificate: UIButton!
    @IBOutlet var pageControl: UIPageControl!
       

    override func viewDidLoad() {
        super.viewDidLoad()
        CheckToken.checkToken()
        activityIndicator.isHidden = true
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationController?.isNavigationBarHidden = false
        flag = getImage(named: countries[0].flag)!
        preAdminName.initMenu()
        picker.delegate = self

    }
    
    func openPicker() {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if self.button != nil {
                self.button!.contentMode = .scaleAspectFit
                self.button!.setBackgroundImage(chosenImage, for: .normal)
                self.button!.layer.masksToBounds = true
                self.button = nil
                self.dismiss(animated:true, completion: nil)
            } else {
                self.pharmacyAvatar.layer.masksToBounds = true
                let avatar = UIImageView(frame: CGRect(x: (0.1 * self.pharmacyAvatar.frame.width), y: (0.1 * self.pharmacyAvatar.frame.height), width: (self.pharmacyAvatar.frame.width * 0.8), height: (self.pharmacyAvatar.frame.height * 0.8)))
                avatar.layer.cornerRadius = 0.4 * self.pharmacyAvatar.frame.height
                avatar.contentMode = .scaleAspectFill
                avatar.clipsToBounds = true
                avatar.layer.borderWidth = 2.0
                avatar.layer.borderColor = UIColor.white.cgColor
                avatar.image = chosenImage
                self.pharmacyAvatar.addSubview(avatar)
                self.dismiss(animated:true, completion: nil)
                
            }
            
        }

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func pharmacyAvatarChange(_ sender: UIButton) {
        openPicker()
    }
    
    
    @IBAction func openMap(_ sender: UIButton) {
        let currentStoryboard = UIStoryboard(name: "Map", bundle: nil)
        let mapController = currentStoryboard.instantiateViewController(withIdentifier: "Map") as! MapVC
        self.navigationController?.pushViewController(mapController, animated: true)
        mapController.delegate = self
    }
    

    @IBAction func IDBtn(sender: UIButton) {
        button = ID
        openPicker()
        
    }
    
    
    @IBAction func practiceCertificateBtn(sender: UIButton) {
        button = practiceCertificate
        openPicker()
    }
    
    
    @IBAction func pharmacyLicenseBtn(sender: UIButton) {
        button = pharmacyLicense
        openPicker()
    }
    
    
    @IBAction func besinessPermitBtn(sender: UIButton) {
        button = besinessPermit
        openPicker()
    }
    
    
    @IBAction func pinCertificateBtn(sender: UIButton) {
        button = pinCertificate
        openPicker()
    }
    

    @IBAction func Next(sender: UIButton) {
//        activityIndicator.startAnimating()
//        activityIndicator.isHidden = false
//        let params = [
//            "user_role" : "3",
//            "country_code" : "by",
//            "latitude" : "53.9180822",
//            "longitude" : "27.5899455",
//            "email" : self.email.text!,
//            "pharmacy_name" : self.phamcacyName.text!,
//            "physical_address" : "Can not get Address", //self.physicalAddress.text!,
//            "name_prefix" : "Mr" ,
//            "avatar" : self.url,
//            "admin_name" : self.adminName.text!,
//            "id_number" : self.url,
//            "phone_code" : self.phoneCode.text!,
//            "phone_number" : self.phone.text!,
//            "practice_certificate" : self.url,
//            "license_of_pharmacy" : self.url,
//            "business_permit" : self.url,
//            "pin_certificate" : self.url,
//            ] as [String :  String]
//        var errorMessage = ""
//        if self.pinCertificate.currentBackgroundImage != nil {
//            UploadImage.uploadImage(selectedImage: pinCertificate.currentBackgroundImage!, success: { (response) -> Void in
//                self.url = response
//            }) { (error) -> Void in
//                errorMessage = (error as? String)!
//            }
//        }
//        if self.pinCertificate.currentBackgroundImage != nil {
//            UploadImage.uploadImage(selectedImage: pinCertificate.currentBackgroundImage!, success: { (response) -> Void in
//            }) { (error) -> Void in
//                errorMessage = (error as? String)!
//            }
//        }
//        if self.pinCertificate.currentBackgroundImage != nil {
//            UploadImage.uploadImage(selectedImage: pinCertificate.currentBackgroundImage!, success: { (response) -> Void in
//            }) { (error) -> Void in
//                errorMessage = (error as? String)!
//            }
//        }
//        if self.pinCertificate.currentBackgroundImage != nil {
//            UploadImage.uploadImage(selectedImage: pinCertificate.currentBackgroundImage!, success: { (response) -> Void in
//            }) { (error) -> Void in
//                errorMessage = (error as? String)!
//            }
//        }
//        if self.pinCertificate.currentBackgroundImage != nil {
//            UploadImage.uploadImage(selectedImage: pinCertificate.currentBackgroundImage!, success: { (response) -> Void in
//            }) { (error) -> Void in
//                errorMessage = (error as? String)!
//
//            }
//        }
//        if errorMessage != "" {
//            self.activityIndicator.stopAnimating()
//            self.activityIndicator.isHidden = true
//            let alert = UIAlertController(title: "Error", message: ("Problem with upload images." + errorMessage), preferredStyle: .alert)
//            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//            alert.addAction(okAction)
//        } else {
//
//            print ("URL out", self.url)
//
//
//            CreatingProfile.createProfile(params: params, success: {
//                 self.activityIndicator.stopAnimating()
//                 self.activityIndicator.isHidden = true
//                print("Ready")
//            }, failure: <#(String) -> Void#>)
////            self.activityIndicator.stopAnimating()
////            self.activityIndicator.isHidden = true
////            let alert = UIAlertController(title: "Error", message: error as? String, preferredStyle: .alert)
////            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
////            alert.addAction(okAction)
//        }
//         print(url)
//    }
    }
    
    
}
