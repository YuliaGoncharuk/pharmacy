//
//  LoadImageFromURL.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 30.09.17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit

func loadImageFromURL(url: String) -> UIImage {
    var image = UIImage()
    if let url = NSURL(string: url) {
        if let data = NSData(contentsOf: url as URL) {
            if let loadedImage = UIImage(data: data as Data) {
                image = loadedImage
            }
        }
    }
    return(image)
}
