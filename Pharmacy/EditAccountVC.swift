//
//  EditAccount.swift
//  Pharmacy
//
//  Created by 123 on 9/28/17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift


class EditAccountVC: UIViewController, SetAddress, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func newAddress(text: String?, longitude: String?, latitude: String?) {
        physicalAddress.text = text
        lat = latitude!
        long = longitude!
    }
    
    let picker = UIImagePickerController()
    var selectedButton : UIButton?
    var lat = ""
    var long = ""

    var buttonName = ""
    var flag = UIImage()
    let params = try! Realm().objects(MainParametres.self)
    let countries = try! Realm().objects(Countries.self)
    var  correctParams :[String: String] = [:]
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var pharmacyAvatar: UIImageView!
    @IBOutlet var phamcacyName: UITextField!
    @IBOutlet var physicalAddress: UITextField!
    @IBOutlet weak var preAdminName: PreNamesList!
    @IBOutlet var adminName: UITextField!
    @IBOutlet var email: UITextField!
    @IBOutlet var phoneCode: UITextField!
    @IBOutlet var phone: UITextField!
    @IBOutlet var ID: UIButton!
    @IBOutlet var practiceCertificate: UIButton!
    @IBOutlet var pharmacyLicense: UIButton!
    @IBOutlet var besinessPermit: UIButton!
    @IBOutlet var pinCertificate: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        activityIndicator.isHidden = true
        //navigationController?.navigationBar.topItem?.rightBarButtonItem?.image = UIImage(named: "ok.png")
        phamcacyName.text = params[0].pharmacyName
        phoneCode.text = params[0].phoneCode
        phone.text = params[0].phoneNumber
        email.text = params[0].email
        adminName.text = params[0].adminName
        physicalAddress.text = params[0].adress
        flag = getImage(named: countries[0].flag)!
        CheckToken.checkToken()
        preAdminName.initMenu()
        picker.delegate = self
        lat = params[0].latitude
        long = params[0].longitude
        let link = "https://test.liviaapp.com" + params[0].avatar
        pharmacyAvatar.setImageFromURl(stringImageUrl: link)
        checkImages()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
       // params[0].adminName = adminName.text!
        //params[0].avatar = pharmacyAvatar.image
    }
    
  
    
    func checkImages() {
        print("DB parametres", params)
        if params[0].idNumber != "" {
            let url = "https://test.liviaapp.com" + params[0].idNumber
            let image = loadImageFromURL(url: url)
            setButtonBackground(button: ID, image: image)
        }
        if params[0].practiceCertificate != "" {
            let url = "https://test.liviaapp.com" + params[0].practiceCertificate
            let image = loadImageFromURL(url: url)
            setButtonBackground(button: practiceCertificate, image: image)
        }
        if params[0].license != "" {
            let url = "https://test.liviaapp.com" + params[0].license
            let image = loadImageFromURL(url: url)
            setButtonBackground(button: pharmacyLicense, image: image)
        }
        if params[0].businessPermin != "" {
            let url = "https://test.liviaapp.com" + params[0].businessPermin
            let image = loadImageFromURL(url: url)
            setButtonBackground(button: besinessPermit, image: image)
        }
        if params[0].pinCertificate != "" {
            let url = "https://test.liviaapp.com" + params[0].pinCertificate
            let image = loadImageFromURL(url: url)
            setButtonBackground(button: pinCertificate, image: image)
           
        }
    }
    
    
    func openPicker() {
    picker.allowsEditing = false
    picker.sourceType = .photoLibrary
    picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
    present(picker, animated: true, completion: nil)
    
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if self.selectedButton != nil {
                setButtonBackground(button: self.selectedButton!, image: chosenImage)
                self.dismiss(animated:true, completion: nil)
            } else {
                self.pharmacyAvatar.layer.masksToBounds = true
                let avatar = UIImageView(frame: CGRect(x: (0.1 * self.pharmacyAvatar.frame.width), y: (0.1 * self.pharmacyAvatar.frame.height), width: (self.pharmacyAvatar.frame.width * 0.8), height: (self.pharmacyAvatar.frame.height * 0.8)))
                avatar.layer.cornerRadius = 0.4 * self.pharmacyAvatar.frame.height
                avatar.contentMode = .scaleAspectFill
                avatar.clipsToBounds = true
                avatar.layer.borderWidth = 2.0
                avatar.layer.borderColor = UIColor.white.cgColor
                avatar.image = chosenImage
                self.pharmacyAvatar.addSubview(avatar)
                self.dismiss(animated:true, completion: nil)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func pharmacyAvatarChange(_ sender: UIButton) {
        openPicker()
    }
    
    func setButtonBackground(button: UIButton, image: UIImage) {
        //image = flag
        button.contentMode = .scaleAspectFit
        button.setBackgroundImage(image, for: .normal)
        button.layer.masksToBounds = true
        self.selectedButton = nil
        UploadImage.uploadImage(selectedImage: flag, success: { (response) -> Void in
            self.correctParams[self.buttonName] = response
            }) { (error) -> Void in
                print(error)
            }
    }
    
    @IBAction func openMap(_ sender: UIButton) {
        let currentStoryboard = UIStoryboard(name: "Map", bundle: nil)
        let mapController = currentStoryboard.instantiateViewController(withIdentifier: "Map") as! MapVC
        self.navigationController?.pushViewController(mapController, animated: true)
        mapController.lat = Double(params[0].latitude)!
        mapController.long = Double(params[0].longitude)!
        mapController.pharmacyAddress = params[0].adress
        mapController.delegate = self
    }
    
    
    @IBAction func IDBtn(sender: UIButton) {
        selectedButton = ID
        buttonName = "id_number"
        openPicker()
    }
    
    
    @IBAction func practiceCertificateBtn(sender: UIButton) {
        selectedButton = practiceCertificate
        buttonName = "practice_certificate"
        openPicker()
    }
    
    
    @IBAction func pharmacyLicenseBtn(sender: UIButton) {
        selectedButton = pharmacyLicense
        buttonName = "license_of_pharmacy"
        openPicker()
    }
    
    
    @IBAction func besinessPermitBtn(sender: UIButton) {
        selectedButton = besinessPermit
        buttonName = "business_permit"
        openPicker()
    }
    
    
    @IBAction func pinCertificateBtn(_ sender: UIButton) {
        selectedButton = pinCertificate
        buttonName = "pin_certificate"
        openPicker()
    }
    
    
 
    
    @IBAction func Save(_ sender: UIBarButtonItem) {
        
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
           correctParams["user_role"] = "3"
            correctParams["country_code"] = "by"
             correctParams["latitude"] = self.lat
             correctParams["longitude"] = self.long
              correctParams["email"] = self.email.text!
              correctParams["pharmacy_name"] = self.phamcacyName.text!
              correctParams["physical_address"] = self.physicalAddress.text!
              correctParams["name_prefix"] = "Mr"
               correctParams["avatar"] = params[0].avatar
               correctParams["admin_name"] = self.adminName.text!
               correctParams["phone_code"] = self.phoneCode.text!
            correctParams["phone_number"] =  self.phone.text!
               
        
        
      
        
        
        
        }
}

