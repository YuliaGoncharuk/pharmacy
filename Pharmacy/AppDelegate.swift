//
//  AppDelegate.swift
//  Pharmacy
//
//  Created by 123 on 9/21/17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import RealmSwift
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let parametresOfUser = try! Realm().objects(ParametresOfUser.self)
    var userStatus = ""

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        IQKeyboardManager.sharedManager().keyboardDistanceFromTextField = 100
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true

        
        GMSServices.provideAPIKey("AIzaSyD4PzXaqgk84BO-d2LoUG0Q6M04KHuehTs")
        GMSPlacesClient.provideAPIKey("AIzaSyD4PzXaqgk84BO-d2LoUG0Q6M04KHuehTs")
        //userStatus = parametresOfUser[0].userStatus
        if parametresOfUser.count > 0 {
            userStatus = parametresOfUser[0].userStatus
        } else {
            userStatus = "6"
        }
            switch userStatus {
        case "1" :
            let storyboard = UIStoryboard(name: "PharmacyAccount", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "PharmacyAccountNavigationController")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        case "3":
            let storyboard = UIStoryboard(name: "Moderation", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "ModerationNavigationController")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        default:
            let storyboard = UIStoryboard(name: "Start", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "StartNavigationController")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }
        return true
    }
//    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//
//        GMSServices.provideAPIKey("AIzaSyD4PzXaqgk84BO-d2LoUG0Q6M04KHuehTs")
//
//        if params.count > 0 {
//
//
//            let storyboard = UIStoryboard(name: "Account", bundle: nil)
//            let initialViewController = storyboard.instantiateViewController(withIdentifier: "AccountNavigationController")
//            self.window?.rootViewController = initialViewController
//            self.window?.makeKeyAndVisible()
//        }
//        else {
//
//            let storyboard = UIStoryboard(name: "Start", bundle: nil)
//            let initialViewController = storyboard.instantiateViewController(withIdentifier: "StartNavigationController")
//            self.window?.rootViewController = initialViewController
//            self.window?.makeKeyAndVisible()
//        }
//        return true
//    }
//
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

