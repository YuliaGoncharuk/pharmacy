//
//  WorkingHoursCell.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 01.10.17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit

class WorkingDaysCell: UITableViewCell {
    
    
    @IBOutlet weak var imageDaysOfWeek: UIImageView!
    @IBOutlet weak var dayName: UILabel!
    @IBOutlet weak var workingHours: UILabel!
    @IBOutlet weak var lunchBreak: UILabel!
    @IBOutlet weak var editBtn: UIImageView!
    
    
    
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

}
