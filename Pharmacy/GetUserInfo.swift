//
//  GetUserInfo.swift
//  Pharmacy
//
//  Created by 123 on 9/29/17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class GetUserInfo {
    static func getUserInfo (success: @escaping (_ response: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        let token = try! Realm().objects(Token.self)
        let accessToken = token[token.count-1].accessToken
        var list :[String: Any] = [:]
        let ob = MainParametres()
        let header = [
            "LiviaApp-APIVersion" : "2.0" ,
            "LiviaApp-Token" : accessToken,
                   ]
        Alamofire.request("https://test.liviaapp.com/api/user?method=1", method: .get, encoding:JSONEncoding.default, headers: header).validate().responseJSON { responseJSON in
            switch responseJSON.result {
            case .success:
                if let json = responseJSON.result.value {
                    print(json)
                    let realm = try! Realm()
                    try! realm.write {
                        realm.delete(realm.objects(MainParametres.self))
                    }
                    list = json as! [String: Any]
                    ob.userRole = list["role_name"] as! String
                    ob.email = list["email"] as! String
                    ob.country_code = list["country_code"] as! String
                    ob.avatar = list["avatar"] as! String
                    ob.latitude = list["latitude"] as! String
                    ob.longitude = list["longitude"] as! String
                    ob.pharmacyName = list["pharmacy_name"] as! String
                    ob.adress = list["physical_address"] as! String
                    ob.adminName = list["admin_name"] as! String
                    ob.idNumber = list["id_number"] as! String
                    ob.userStatus = "2"
                    ob.online = list["online"] as! String
                    if let practiceCertificate = list["practice_certificate"] as? String {
                        ob.practiceCertificate = practiceCertificate
                    }
                    if let license = list["license_of_pharmacy"] as? String {
                        ob.license = license
                    }
                    if let businessPermin = list["business_permit"] as? String {
                        ob.businessPermin = businessPermin
                    }
                    if let pinCertificate = list["pin_certificate"] as? String {
                        ob.pinCertificate = pinCertificate
                    }
                    ob.phoneCode = list["phone_code"] as! String
                    ob.phoneNumber = list["phone_number"] as! String
                    try! realm.write {
                        realm.add(ob, update: true)
                    }
                }

                success(list)
            case .failure(let error):
                print(error)
                failure(error)
            }
        }
    }
}
