//
//  RegistrationViewController.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 22.09.17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire

class RegistrationViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var registrationStackView: UIStackView!
    @IBOutlet weak var phoneNumberLabel: UITextField!
    @IBOutlet var chooseCountry:DropMenuButton!
    
    let countries = try! Realm().objects(Countries.self)
    var params : [String: String] = [:]
    var authCode = 0
    var count = -1
    enum options : Int {
        case login
        case registration
    }
    
    var userOption = options.registration

    
    private lazy var backgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10.0
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "background.jpg")
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
        
        stackBackground(backgroundView, to: registrationStackView)

        chooseCountry.initMenu()
    }
    
    private func stackBackground(_ view: UIView, to stackView: UIStackView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        stackView.insertSubview(view, at: 0)
        view.pin(to: stackView)
    }
    
    @IBAction func nextButton(_ sender: UIButton) {
        activityIndicator.startAnimating()

        var params = PostAndGetCode.getCode(phoneNumber: phoneNumberLabel.text!, completion: {
            print("Ready!")
            self.activityIndicator.stopAnimating()
        })
        
        let storyboard = UIStoryboard(name: "Confirm", bundle: nil)
        let confirmController = storyboard.instantiateViewController(withIdentifier :"ConfirmRegistration") as! ConfirmController
        self.navigationController?.pushViewController(confirmController, animated: true)
        confirmController.paramsDictionary = params
        
    }
    

}

// White background for stack view
extension UIView {
    public func pin(to view: UIView) {
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: view.leadingAnchor),
            trailingAnchor.constraint(equalTo: view.trailingAnchor),
            topAnchor.constraint(equalTo: view.topAnchor),
            bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
}
