//
//  DataManager.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 23.09.17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit

//// Load image from websute and call function to save it to directory
//extension UIImageView{
//    func setImageFromURl(stringImageUrl url: String){
//
//        if let url = NSURL(string: url) {
//            if let data = NSData(contentsOf: url as URL) {
//                let image = UIImage(data: data as Data)
////                saveImage(image: image!)
//            }
//        }
//    }
//}


// Save image to directory
func saveImage(image: UIImage, name: String) -> Bool {
    guard let data = UIImageJPEGRepresentation(image, 1) ?? UIImagePNGRepresentation(image) else {
        return false
    }
    guard let directory = try? FileManager.default.url(for: .documentDirectory , in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
        return false
    }
    do {
        try data.write(to: directory.appendingPathComponent(name)!)
        return true
    } catch {
        print(error.localizedDescription)
        return false
    }
}


// Load image from directory
func getImage(named: String) -> UIImage? {
    if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
        return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
    }
    return nil
}


