//
//  PostAndGetCode.swift
//  Pharmacy
//
//  Created by 123 on 9/25/17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class PostAndGetCode {
    static  func getCode(phoneNumber:  String, phoneCode:String, success: @escaping (_ response: Dictionary<String, String>) -> Void, failure: @escaping (_ error: String) -> Void) {
        
        let ob = ParametresOfUser()
        ob.phoneIdNumber = UIDevice.current.identifierForVendor!.uuidString
        ob.phoneNumber = phoneNumber
        ob.phoneCode = phoneCode
        ob.userRole = "3"
        let header = [ "LiviaApp-APIVersion" : "2.0" ]
        let params = [
                "phone_id": ob.phoneIdNumber,
                "phone_code": ob.phoneCode,
                "phone_number": ob.phoneNumber,
                "user_role" : ob.userRole
            ]
        
        Alamofire.request("https://test.liviaapp.com/api/auth", method: .post, parameters: params, headers: header).validate().responseJSON { responseJSON in
            switch responseJSON.result {
            case .success:
                if let json = responseJSON.result.value {
                   print(json)
                    let list = json as! [String: Any]
                    ob.userStatus = list["user_status"] as! String
                  // ob.userStatus = String(userStatus
                    let realm = try! Realm()
                    try! realm.write {
                        realm.delete(realm.objects(ParametresOfUser.self))
                    }
                    try! realm.write {
                        realm.add(ob, update:true)
                    }
                }
                success(params)
            case .failure(let error):
                let errorMessage = error.localizedDescription
                failure(errorMessage)
            }
        }
    }
    
    
    static func checkCode(params: Dictionary<String, String>, success: @escaping (_ response: String) -> Void, failure: @escaping (_ error: String) -> Void) {
        let header = [ "LiviaApp-APIVersion" : "2.0" ]
        let token = Token()
        request("https://test.liviaapp.com/api/auth", method: .put, parameters: params, headers: header).validate().responseJSON { responseJSON in
         switch responseJSON.result {
            case .success:
                if let json = responseJSON.result.value {
                    print(json)
                    let list = json as! [String: Any]
                    token.accessToken = list["access_token"] as! String
                    token.createdTime = Int(Date().timeIntervalSince1970)
                    token.refreshToken = list["refresh_token"] as! String
                    let realm = try! Realm()
                    try! realm.write {
                        realm.delete(realm.objects(Token.self))
                    }
                    try! realm.write {
                        realm.add(token)
                    }
                }
                success(token.accessToken)
            case .failure(let error):
                let errorMessage = error.localizedDescription
                failure(errorMessage)
            }
        }
    }
}

