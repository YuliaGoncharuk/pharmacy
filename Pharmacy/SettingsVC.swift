//
//  SettingsVC.swift
//  Pharmacy
//
//  Created by 123 on 9/30/17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import RealmSwift

class SettingsVC: UIViewController {

    let backgroundImage = UIImageView(frame: UIScreen.main.bounds)

    override func viewDidLoad() {
        super.viewDidLoad()
        CheckToken.checkToken()
        self.navigationController?.isNavigationBarHidden = false
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "accountOnlineBackground.jpg")
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFit
        self.view.insertSubview(backgroundImage, at: 0)
        
    }
    
    
    @IBAction func logoutFromOneBtn(_ sender: UIButton) {
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(ParametresOfUser.self))
        }
        try! realm.write {
            realm.delete(realm.objects(Token.self))
        }
        try! realm.write {
            realm.delete(realm.objects(MainParametres.self))
        }
        let currentStoryboard = UIStoryboard(name: "Start", bundle: nil)
        let createAccountController = currentStoryboard.instantiateViewController(withIdentifier: "Start") as! StartVC
        self.navigationController?.pushViewController(createAccountController, animated: true)
    }


    
   
    
}

