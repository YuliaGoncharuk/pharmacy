//
//  workingDays.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 01.10.17.
//  Copyright © 2017 123. All rights reserved.
//


import Foundation
import RealmSwift


class WorkingDays : Object {
    
    dynamic var dayInWeek = 0
    dynamic var dayType = 0
    dynamic var startWork = ""
    dynamic var endWork = ""
    dynamic var startLunch = ""
    dynamic var endLunch = ""
    
    override static func primaryKey() -> String? {
        return "dayInWeek"
    }
}

