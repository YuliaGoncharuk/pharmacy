//
//  StartViewController.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 22.09.17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit

class StartVC: UIViewController {
    
    
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var registrationBtn: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "start.jpg")
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFit
        self.view.insertSubview(backgroundImage, at: 0)
        LoadCountries.loadData()
    }
    
     override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        loginBtn.fade()
        registrationBtn.fade()
    }
}

extension UIButton {
    func fade() {
        let fade = CABasicAnimation(keyPath: "opacity")
        fade.duration = 0.5
        fade.fromValue = 0
        fade.toValue = 1
        fade.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        layer.add(fade, forKey: nil)
    }
}
