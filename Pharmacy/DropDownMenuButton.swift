//
//  DropDownMenuButton.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 22.09.17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import RealmSwift


class DropMenuButton: UIButton, UITableViewDelegate, UITableViewDataSource {
    
    
    let countries = try! Realm().objects(Countries.self).sorted(byKeyPath: "name")
    var table = UITableView()
    var act = [() -> (Void)]()
    var superSuperView = UIView()
    var phoneCode = ""
    var digitsPhoneNumber = 0
    
    func showItems() {
        fixLayout()
        
        if(table.alpha == 0) {
            self.layer.zPosition = 1
            UIView.animate(withDuration: 0.3
                , animations: {
                    self.table.alpha = 1;
            })
            
        } else {
            UIView.animate(withDuration: 0.3
                , animations: {
                    self.table.alpha = 0;
                    self.layer.zPosition = 0
            })
        }
    }
    

    func initMenu() {
        
        self.setTitle("", for: UIControlState())
        
        
        let name = UILabel(frame: CGRect(x: 3, y: 0, width: self.frame.width, height: (self.frame.height / 2)))
        name.text = countries[0].name
        name.font = self.titleLabel?.font
        name.textColor = UIColor.darkGray
        self.addSubview(name)
        
        let flag = UIImageView(frame: CGRect(x: 0, y: 25, width: (self.frame.width / 2), height: (self.frame.height / 2)))
        flag.contentMode = .scaleAspectFit
        flag.image = getImage(named: countries[0].flag)
        self.addSubview(flag)
        
        let phone_country_code = UILabel(frame: CGRect(x: 45, y: 25, width: (self.frame.width / 2), height: (self.frame.height / 2)))
        phone_country_code.textAlignment = NSTextAlignment.left
        phone_country_code.text = String(countries[0].phone_country_code)
        phone_country_code.font = self.titleLabel?.font
        phone_country_code.textColor = UIColor.darkGray
        self.addSubview(phone_country_code)
        
        var resp = self as UIResponder
        
        while !(resp.isKind(of: UIViewController.self) || (resp.isKind(of: UITableViewCell.self))) && resp.next != nil {
            resp = resp.next!
        }
        
        if let vc = resp as? UIViewController{
            superSuperView = vc.view
        } else if let vc = resp as? UITableViewCell{
            superSuperView = vc
        }
        
        table = UITableView()
        table.rowHeight = self.frame.height
        table.delegate = self
        table.dataSource = self
        table.isUserInteractionEnabled = true
        table.alpha = 0
        table.separatorColor = self.backgroundColor
        superSuperView.addSubview(table)
        superSuperView.backgroundColor = .red
        self.addTarget(self, action:#selector(DropMenuButton.showItems), for: .touchUpInside)
    }
    
    func initMenu(_ items: [String]) {
        var resp = self as UIResponder
        while !(resp.isKind(of: UIViewController.self) || (resp.isKind(of: UITableViewCell.self))) && resp.next != nil {
            resp = resp.next!
        }
        
        if let vc = resp as? UIViewController {
            superSuperView = vc.view
        }
        else if let vc = resp as? UITableViewCell {
            superSuperView = vc
        }
        
        table = UITableView()
        table.rowHeight = self.frame.height
        table.delegate = self
        table.dataSource = self
        table.isUserInteractionEnabled = true
        table.alpha = 0
        table.separatorColor = self.backgroundColor
        superSuperView.addSubview(table)
        self.addTarget(self, action:#selector(DropMenuButton.showItems), for: .touchUpInside)
    }
    
    
    func fixLayout() {
        let auxPoint2 = superSuperView.convert(self.frame.origin, from: self.superview)
        var tableFrameHeight = CGFloat()
        tableFrameHeight = self.frame.height * 4
        table.frame = CGRect(x: auxPoint2.x, y: auxPoint2.y + self.frame.height, width: self.frame.width, height:tableFrameHeight)
        table.rowHeight = self.frame.height
        table.reloadData()
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setNeedsDisplay()
        fixLayout()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for subUIView in self.subviews as [UIView] {
            subUIView.removeFromSuperview()
        }
        
        let phoneNumberLine = UIView(frame: CGRect(x: 0, y: self.frame.height, width: self.frame.size.width, height: 2))
        phoneNumberLine.backgroundColor = UIColor.lightGray
        self.addSubview(phoneNumberLine)
        
        let name = UILabel(frame: CGRect(x: 3, y: 0, width: self.frame.width, height: (self.frame.height / 2)))
        name.text = countries[indexPath.row].name
        name.font = self.titleLabel?.font
        name.textColor = UIColor.darkGray
        self.addSubview(name)
        
        let flag = UIImageView(frame: CGRect(x: 0, y: 25, width: (self.frame.width / 2), height: (self.frame.height / 2)))
        flag.contentMode = .scaleAspectFit
        flag.image = getImage(named: countries[indexPath.row].flag)
        self.addSubview(flag)

        let phone_country_code = UILabel(frame: CGRect(x: 45, y: 25, width: (self.frame.width / 2), height: (self.frame.height / 2)))
        phone_country_code.textAlignment = NSTextAlignment.left
        phone_country_code.text = String(countries[indexPath.row].phone_country_code)
        phone_country_code.font = self.titleLabel?.font
        phone_country_code.textColor = UIColor.darkGray
        self.addSubview(phone_country_code)
        
        digitsPhoneNumber = countries[indexPath.row].digits_phone_number
        phoneCode = countries[indexPath.row].phone_country_code
        
        if self.act.count > 1 {
            self.act[indexPath.row]()
        }
        showItems()
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let name = UILabel(frame: CGRect(x: 3, y: 0, width: self.frame.width, height: (self.frame.height / 2)))
        name.textAlignment = NSTextAlignment.left
        name.text = countries[indexPath.row].name
        name.font = self.titleLabel?.font
        name.textColor = UIColor.darkGray
        
        let flag = UIImageView(frame: CGRect(x: 0, y: 25, width: (self.frame.width / 2), height: (self.frame.height / 2)))
        flag.contentMode = .scaleAspectFit
        flag.image = getImage(named: countries[indexPath.row].flag)
        
        
        let phone_country_code = UILabel(frame: CGRect(x: 45, y: 25, width: (self.frame.width / 2), height: (self.frame.height / 2)))
        phone_country_code.textAlignment = NSTextAlignment.left
        phone_country_code.text = String(countries[indexPath.row].phone_country_code)
        phone_country_code.font = self.titleLabel?.font
        phone_country_code.textColor = UIColor.darkGray
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.white
        
        let cell = UITableViewCell(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        cell.backgroundColor = .white
        cell.selectedBackgroundView = bgColorView
        cell.separatorInset = UIEdgeInsetsMake(0, self.frame.width, 0, self.frame.width)
        cell.addSubview(name)
        cell.addSubview(flag)
        cell.addSubview(phone_country_code)
        
        return cell
    }
}

