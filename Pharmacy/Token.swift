//
//  Token.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 27.09.17.
//  Copyright © 2017 123. All rights reserved.
//

import Foundation
import RealmSwift



class Token : Object {
    dynamic var accessToken = ""
    dynamic var refreshToken = ""
    dynamic var createdTime = 0
}
