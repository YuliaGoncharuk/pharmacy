//
//  UploadImage.swift
//  Pharmacy
//
//  Created by 123 on 9/26/17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class UploadImage {
    static func uploadImage (selectedImage: UIImage, success: @escaping (_ response: String) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        var url = ""
        let imageData = UIImagePNGRepresentation(selectedImage)! as NSData
        let imageStr = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        let params = [
            "image" : imageStr,
            "type" : "users"
            ] as [String : String]
        let tokens = try! Realm().objects(Token.self)
        let accessToken = tokens[tokens.count-1].accessToken
        let header = [ "LiviaApp-APIVersion" : "2.0" ,
                       "LiviaApp-Token" : accessToken,
                     ]
        Alamofire.request("https://test.liviaapp.com/api/image", method: .post, parameters: params,encoding:JSONEncoding.default, headers: header).validate().responseJSON { responseJSON in
            switch responseJSON.result {
            case .success:
                if let json = responseJSON.result.value {
                    let list = json as! [String: Any]
                    url = list["image"] as! String!
                    
                }
                success(url)
            case .failure(let error):
                print(error)
                failure(error)
            }
        }
    }
}



