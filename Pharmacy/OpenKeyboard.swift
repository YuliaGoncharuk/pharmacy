////
////  OpenKeyboard.swift
////  Pharmacy
////
////  Created by Mikhail Filimonov on 28.09.17.
////  Copyright © 2017 123. All rights reserved.
////
//
//import UIKit
//
//
//public class OpenKeyboard {
//    
//    var activeField: UITextField?
//    
//    
////    override func viewDidAppear(_ animated: Bool) {
////        addKeyboardObservers()
////    }
////    
////    override func viewWillDisappear(_ animated: Bool) {
////        super.viewWillDisappear(animated)
////        
////        removeKeyboardObservers()
////    }
//    
//    func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        view.endEditing(true)
//    }
//
//    func addKeyboardObservers() {
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//    }
//    
//    func removeKeyboardObservers() {
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
//    }
//    
//    func textFieldDidBeginEditing(_ textField: UITextField){
//        activeField = textField
//    }
//    
//    func textFieldDidEndEditing(_ textField: UITextField){
//        activeField = nil
//    }
//    
//    func keyboardWillShow(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            self.view.frame.origin.y = self.view.frame.origin.y - keyboardSize.height
////                + 100 - (self.activeField?.frame.origin.y)!)
//            self.view.layoutIfNeeded()
//            
////            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
//        }
//        
//    }
//    
//    func keyboardWillHide(notification: NSNotification) {
//        self.view.frame.origin.y = 0
//        self.view.layoutIfNeeded()
//    }
//    
//
//    func resignTextFieldFirstResponders() {
//        for textField in self.view.subviews where textField is UITextField {
//            textField.resignFirstResponder()
//        }
//    }
//    
//    func resignAllFirstResponders() {
//        view.endEditing(true)
//    }
//}

