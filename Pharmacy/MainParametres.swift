//
//  MainParametres.swift
//  Pharmacy
//
//  Created by 123 on 9/26/17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import RealmSwift

class MainParametres : Object {
    
    dynamic var userRole = ""
    dynamic var avatar = ""
    dynamic var email = ""
    dynamic var country_code = ""
    dynamic var latitude = ""
    dynamic var longitude = ""
    dynamic var pharmacyName = ""
    dynamic var adress = ""
    dynamic var adminName = ""
    dynamic var idNumber = ""
    dynamic var userStatus = ""
    dynamic var online = ""
    dynamic var practiceCertificate = ""
    dynamic var license = ""
    dynamic var businessPermin = ""
    dynamic var pinCertificate = ""
    dynamic var phoneCode = ""
    dynamic var phoneNumber = ""
    
    override static func primaryKey() -> String? {
        return "idNumber"
    }
}
