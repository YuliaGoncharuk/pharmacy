//
//  RegistrationViewController.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 22.09.17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire

//class RegistrationViewController: UIViewController, UITextFieldDelegate, ChangePhoneCode {

class RegistrationViewController: UIViewController, UITextFieldDelegate {
    
    

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var registrationStackView: UIStackView!
    @IBOutlet weak var phoneNumberLabel: UITextField!
    @IBOutlet var chooseCountry:DropMenuButton!
    
    
//    func codeChanged(name: String) {
//        var newName:String?
//        newName = name
//        countryName.text = newName
//    }
    
    
    let countries = try! Realm().objects(Countries.self)
    let dropDownMenu = DropMenuButton()
    var params : [String: String] = [:]
    var authCode = 0
    var count = -1
    enum options : Int {
        case login
        case registration
    }
    
    var userOption = options.registration

    
    private lazy var backgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10.0
        return view
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        if userOption == options.login {
            self.title = "Login"
        }
        
        self.phoneNumberLabel.delegate = self
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "background.jpg")
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
        
        stackBackground(backgroundView, to: registrationStackView)

        chooseCountry.initMenu()
    }
    
    
    // Set white background for stackView
    private func stackBackground(_ view: UIView, to stackView: UIStackView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        stackView.insertSubview(view, at: 0)
        view.pin(to: stackView)
    }
    
<<<<<<< HEAD
    @IBAction func nextButton(_ sender: UIButton) {
        activityIndicator.startAnimating()

        var params = PostAndGetCode.getCode(phoneNumber: phoneNumberLabel.text!, completion: {
            print("Ready!")
            self.activityIndicator.stopAnimating()
        })
        
        let storyboard = UIStoryboard(name: "Confirm", bundle: nil)
        let confirmController = storyboard.instantiateViewController(withIdentifier :"ConfirmRegistration") as! ConfirmController
        self.navigationController?.pushViewController(confirmController, animated: true)
        confirmController.paramsDictionary = params
=======
    
    // We can input only digits in phone number text field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
    
    
    @IBAction func nextButton(_ sender: UIButton) {
        let phoneCode = chooseCountry.phoneCode
        let digitsPhoneNumber = chooseCountry.digitsPhoneNumber
        if (phoneNumberLabel.text?.isEmpty)! || (phoneNumberLabel.text!.characters.count != digitsPhoneNumber) {
            let alert = UIAlertController(title: "Error", message: "Please enter correct phone number", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
        } else {
            activityIndicator.startAnimating()
            activityIndicator.isHidden = false
            params = [
                "phone_id": UIDevice.current.identifierForVendor!.uuidString,
                "phone_code": "\(phoneCode)",
                "phone_number": phoneNumberLabel.text!,
                "user_role" : "3"
            ]
        
            PostAndGetCode.getCode(params: params, completion: {
                print("Ready!")
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
//             self.openConfirmWindow()
            })
>>>>>>> ca64b4c31dcf020a7b4efd5055672a0d0f134baf
        
            let storyboard = UIStoryboard(name: "Confirm", bundle: nil)
            let confirmController = storyboard.instantiateViewController(withIdentifier :"ConfirmRegistration") as! ConfirmController
            self.navigationController?.pushViewController(confirmController, animated: true)
            confirmController.params = params
        }
    }
    
<<<<<<< HEAD
=======

>>>>>>> ca64b4c31dcf020a7b4efd5055672a0d0f134baf

}

// White background for stack view
extension UIView {
    public func pin(to view: UIView) {
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: view.leadingAnchor),
            trailingAnchor.constraint(equalTo: view.trailingAnchor),
            topAnchor.constraint(equalTo: view.topAnchor),
            bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
}
