//
//  ConfirmController.swift
//  Pharmacy
//
//  Created by 123 on 9/25/17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class ConfirmVC: UIViewController {
    
    let paramsDB = try! Realm().objects(ParametresOfUser.self)

    private lazy var backgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10.0
        return view
    }()
    
   var  paramsDictionary : [String: String] = [:]
    
    var  code = 0

    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var confirmStackView: UIStackView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
 
   
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.activityIndicator.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "background.jpg")
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
        
        stackBackground(backgroundView, to: confirmStackView)
        
        let codeTextLine = UIView(frame: CGRect(x: 0, y: codeTextField.bounds.size.height, width: codeTextField.frame.size.width, height: 2))
        codeTextLine.backgroundColor = UIColor.lightGray
        codeTextField.addSubview(codeTextLine)

    }
    
    private func stackBackground(_ view: UIView, to stackView: UIStackView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        stackView.insertSubview(view, at: 0)
        view.background(to: stackView)
    }

    @IBAction func confirmButton(_ sender: UIButton) {
        activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false

        paramsDictionary["auth_code"] = codeTextField.text //paramsDB[0].authCode
        paramsDictionary["os_type"] = "2"
        PostAndGetCode.checkCode(params: paramsDictionary, success: { (response) -> Void in
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
        
            switch self.paramsDB[0].userStatus {
            case "1":
                GetUserInfo.getUserInfo( success: { (response) -> Void in
                    print ("Ready")

                    let currentStoryboard = UIStoryboard(name: "PharmacyAccount", bundle: nil)
                    let createProfileController = currentStoryboard.instantiateViewController(withIdentifier: "Account") as! AccountVC
                    self.navigationController?.pushViewController(createProfileController, animated: true)
                   
      
                }) { (error) -> Void in
                    let alert = UIAlertController(title: "Error", message: error as? String, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alert.addAction(okAction)
                }
               
            case "6":
                let currentStoryboard = UIStoryboard(name: "Profile", bundle: nil)
                let createProfileController = currentStoryboard.instantiateViewController(withIdentifier: "Profile") as! CreateProfileVC
                self.navigationController?.pushViewController(createProfileController, animated: true)
            default:
                let currentStoryboard = UIStoryboard(name: "Moderation", bundle: nil)
                let createProfileController = currentStoryboard.instantiateViewController(withIdentifier: "Moderation") as! ModerationVC
                self.navigationController?.pushViewController(createProfileController, animated: true)
            }
        }) { (error) -> Void in
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            self.errorMessage(errorDescription: error)
        }
    }
    
    func errorMessage(errorDescription: String) {
        let alert = UIAlertController(title: "Error", message: errorDescription, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
}

// White background for stack view
extension UIView {
    public func background(to view: UIView) {
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: view.leadingAnchor),
            trailingAnchor.constraint(equalTo: view.trailingAnchor),
            topAnchor.constraint(equalTo: view.topAnchor),
            bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
}

