//
//  WorkingHoursVCTableViewController.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 01.10.17.
//  Copyright © 2017 123. All rights reserved.
//


import UIKit
import RealmSwift

class WorkingDaysVC: UITableViewController {
    
    let workingDays = try! Realm().objects(WorkingDays.self)
    let daysName = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
    
    @IBOutlet weak var workingDaysTable: UITableView!
    

    override func viewDidLoad() {
        createDays()
        super.viewDidLoad()
//        workingDaysTable.estimatedRowHeight = 70
        
    }
    
    
    func createDays() {
        
        print("working days count", workingDays.count)
        if workingDays.isEmpty {
            for day in  0..<7 {
                let days = WorkingDays()
                days.dayInWeek = day
                days.dayType = 0
                days.startWork = "0"
                days.endWork = "0"
                days.startLunch = "0"
                days.endLunch = "0"
                let realm = try! Realm()
                try! realm.write {
                    realm.add(days, update: true)
                }
            }
        }
    }

    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        print("number of sections")
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print("Number of rows", workingDays.count)
        return (workingDays.count)
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "workingDaysCell", for:indexPath) as! WorkingDaysCell
        print("cell")
        print(workingDays[indexPath.row].dayType)
        if workingDays[indexPath.row].dayType == 0 {
            cell.imageDaysOfWeek.image = #imageLiteral(resourceName: "notWorkingDay")
        } else {
            cell.imageDaysOfWeek.image = #imageLiteral(resourceName: "workingDay")
        }
        cell.dayName?.text = daysName[indexPath.row]
        cell.workingHours.text = "Working " + workingDays[indexPath.row].startWork + " - " + workingDays[indexPath.row].endWork
        cell.lunchBreak.text = "Lunch break " + workingDays[indexPath.row].startLunch + " - " + workingDays[indexPath.row].endLunch
        cell.editBtn.image = #imageLiteral(resourceName: "editWhite")
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
