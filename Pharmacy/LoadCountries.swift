//
//  LoadCountries.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 22.09.17.
//  Copyright © 2017 123. All rights reserved.
//

import Alamofire
import RealmSwift
import UIKit

class LoadCountries {
    static func loadData () {
        print (Realm.Configuration.defaultConfiguration)
        let link = "https://test.liviaapp.com/api/settings"
        Alamofire.request(link).responseJSON { response in
            switch response.result {
            case .success:
                if let json = response.result.value {
                    let listOfCountries = json as! [String: Any]
                    let arrayOfCountries = listOfCountries["list_of_countries"] as! [[String: Any]]
                    
                    for i in 0..<arrayOfCountries.count{
                      let countries = Countries()
                        countries.id = arrayOfCountries[i]["id"] as! String
                        countries.name = arrayOfCountries[i]["name"] as! String
                        countries.phone_country_code = arrayOfCountries[i]["phone_country_code"] as! String
                        countries.digits_phone_number = Int(arrayOfCountries[i]["digits_phone_number"] as! String)!
                        countries.currency_symbol = String(describing: arrayOfCountries[i]["currency_symbol"])
                        let imageLink = "https://admin.liviaapp.com" + String(arrayOfCountries[i]["flag"] as! String)!
                        var imageName = arrayOfCountries[i]["flag"] as! String
                        imageName = imageName.replacingOccurrences(of: "/images/flags/32x32/", with: "")
                        if let url = NSURL(string: imageLink) {
                            if let data = NSData(contentsOf: url as URL) {
                                if let imageCountry = UIImage(data: data as Data) {
                                    if saveImage(image: imageCountry, name: imageName) == true {
                                        countries.flag = imageName
                                    }
                                }
                            }
                        }
                        let realm = try! Realm()
                        try! realm.write {
                        realm.add(countries, update: true)
                        }
                    }
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}



