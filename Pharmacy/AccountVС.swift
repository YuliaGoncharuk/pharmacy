//
//  AccountViewController.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 26.09.17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import RealmSwift

class AccountVC: UIViewController {
    

    let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
    let params = try! Realm().objects(MainParametres.self)
    
    @IBOutlet weak var pharmacyAvatar: UIImageView!
    @IBOutlet weak var pharmacyName: UILabel!
    @IBOutlet weak var statusDescriptionLabel: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var statusSwitch: UISwitch!
    @IBOutlet weak var ordersLabel: UILabel!

    

    

    override func viewDidLoad() {
        super.viewDidLoad()
        CheckToken.checkToken()
        self.navigationController?.isNavigationBarHidden = true
//        changeBackground(name: "accountOnlineBackground")
        statusSwitch.setOn(false, animated: false)
        let link = "https://test.liviaapp.com" + params[0].avatar
        pharmacyAvatar.setImageFromURl(stringImageUrl: link)
        pharmacyName.text = params[0].adminName
        
        switch (params[0].online){
            case "0" :
                backgroundImage.image = UIImage(named: "accountOfflineBackground.jpg")
            default :
                backgroundImage.image = UIImage(named: "accountOnlineBackground.jpg")
        }
        backgroundImage.image = UIImage(named: "accountOfflineBackground.jpg")
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFit
        self.view.insertSubview(backgroundImage, at: 0)
        
        
    }


    @IBAction func editProfile(_ sender: UIButton) {
        
//        let currentStoryboard = UIStoryboard(name: "EditAccount", bundle: nil)
//        let createAccountController = currentStoryboard.instantiateViewController(withIdentifier: "EditAccount") as! EditAccountVC
//        self.navigationController?.pushViewController(createAccountController, animated: true)

    }
    
    
    @IBAction func statusBtn(_ sender: UIButton) {
        if (statusSwitch.isOn == true) {
            statusDescriptionLabel.text = "Not taken orders"
            statusSwitch.setOn(false, animated: true)
            status.text = "Offline"
            backgroundImage.image = UIImage(named: "accountOfflineBackground.jpg")
        }
        else {
            statusDescriptionLabel.text = "Take orders"
            statusSwitch.setOn(true, animated: true)
            status.text = "Online"
            backgroundImage.image = UIImage(named: "accountOnlineBackground.jpg")
        }
    }
    
    @IBAction func ordersBtn(_ sender: UIButton) {
    }
    
    
    @IBAction func deliveryManBtn(_ sender: UIButton) {
    }
    
    
    @IBAction func faqBtn(_ sender: UIButton) {
    }
    
}

extension UIImageView {
    func setImageFromURl(stringImageUrl url: String){

        if let url = NSURL(string: url) {
            if let data = NSData(contentsOf: url as URL) {
                let avatar = UIImageView(frame: CGRect(x: (0.1 * self.frame.width), y: (0.1 * self.frame.height), width: (self.frame.width * 0.8), height: (self.frame.height * 0.8)))
                avatar.image = UIImage(data: data as Data)
                self.layer.masksToBounds = true
                avatar.layer.cornerRadius = 0.4 * self.frame.height
                avatar.contentMode = .scaleAspectFill
                avatar.clipsToBounds = true
                avatar.layer.borderWidth = 2.0
                avatar.layer.borderColor = UIColor.white.cgColor
                self.addSubview(avatar)
            }
        }

    }
}

