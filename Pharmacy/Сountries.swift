//
//  Сountries.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 22.09.17.
//  Copyright © 2017 123. All rights reserved.
//

import Foundation
import RealmSwift


class Countries : Object {
    dynamic var id = ""
    dynamic var name = ""
    dynamic var country_code = ""
    dynamic var status = 0
    dynamic var phone_country_code = ""
    dynamic var digits_phone_number = 0
    dynamic var show_insurance_company = 0
    dynamic var default_currency = ""
    dynamic var currency_symbol = ""
    dynamic var flag = ""
   
    override static func primaryKey() -> String? {
        return "id"
    }
}

