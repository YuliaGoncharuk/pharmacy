//
//  ModerationViewController.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 26.09.17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit

class ModerationVC: UIViewController {
    
    
    @IBOutlet weak var moderationStackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CheckToken.checkToken()
        self.navigationController?.isNavigationBarHidden = true
    }

    
    
    @IBAction func sendMail(_ sender: UIButton) {
        let subject = "Livia app"
        let body = "Hello!"
        let coded = "mailto:blah@blah.com?subject=\(subject)&body=\(body)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        if let emailURL: NSURL = NSURL(string: coded!) {
            if UIApplication.shared.canOpenURL(emailURL as URL) {
                UIApplication.shared.canOpenURL(emailURL as URL)
            }
        }
    }
}
