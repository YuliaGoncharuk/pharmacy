//
//  CheckToken.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 27.09.17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class CheckToken {
    static func checkToken () {
        
        let header = [ "LiviaApp-APIVersion" : "2.0" ]
        let token = try! Realm().objects(Token.self)
        let currentTime = Int(Date().timeIntervalSince1970)
        
        if (currentTime - token[0].createdTime) > 6300 {
            let ob = try! Realm().objects(ParametresOfUser.self)
            let params = [
                "phone_id": ob[ob.count - 1].phoneIdNumber,
                "phone_code": ob[ob.count - 1].phoneCode,
                "phone_number": ob[ob.count - 1].phoneNumber,
                "refresh_token" : token[0].refreshToken,
                "os_type" : "2"
            ]
            
            Alamofire.request("https://test.liviaapp.com/api/auth/1", method: .post, parameters: params, headers: header).validate().responseJSON { responseJSON in
                switch responseJSON.result {
                case .success:
                    if let json = responseJSON.result.value {
                       let realm = try! Realm()
                        try! realm.write {
                            realm.delete(realm.objects(Token.self))
                        }
                        let newToken = Token()
                        let list = json as! [String: Any]
                        newToken.accessToken = list["access_token"] as! String
                        newToken.refreshToken = list["refresh_token"] as! String
                        newToken.createdTime = Int(Date().timeIntervalSince1970)
                        try! realm.write {
                            realm.add(token)
                        }
                    }
           
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
}
