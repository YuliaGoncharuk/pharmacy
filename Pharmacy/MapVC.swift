//
//  MapViewController.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 28.09.17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

protocol SetAddress: class {
    
    func newAddress(text:String?, longitude:String?, latitude:String?)
}

class MapVC: UIViewController, GMSMapViewDelegate {
    
    weak var delegate: SetAddress?
    var locationManager = CLLocationManager()
    var mapView: GMSMapView!
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 15.0
    let geocoder = GMSGeocoder()
    var lat = 0.0
    var long = 0.0
    var pharmacyAddress = ""
    
    
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var googleMap: UIView!

    
    // An array to hold the list of likely places.
    var likelyPlaces: [GMSPlace] = []
    
    // The currently selected place.
    var selectedPlace: GMSPlace?
    
    // A default location to use when location permission is not granted.
    let defaultLocation = CLLocation(latitude: 19.4469756, longitude: 4.4584923)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        openMap()
        address.addTarget(self, action: #selector(MapVC.addressDidChanged), for: UIControlEvents.editingChanged)
        
    }
    
    
    func openMap() {
        let camera = GMSCameraPosition.camera(withLatitude: defaultLocation.coordinate.latitude, longitude: defaultLocation.coordinate.longitude, zoom: 1.0)
        mapView = GMSMapView.map(withFrame: view.bounds, camera: camera)
        mapView.settings.myLocationButton = true
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.delegate = self
        googleMap.addSubview(mapView)
        mapView.delegate = self
        
        if (self.lat != 0.0) && (self.long != 0.0) {
            let coordinate = CLLocationCoordinate2DMake(self.lat, self.long)
            print("Find by coordinates", coordinate)
            geocoder.reverseGeocodeCoordinate(coordinate) { (response, error) in
                guard error == nil else {
                    return
                }
                
                if let result = response?.firstResult() {
                    let marker = GMSMarker()
                    marker.position = coordinate
                    marker.title = result.lines?[0]
                    marker.map = self.mapView
                    self.address.text = marker.title
                    self.mapView.animate(toLocation: CLLocationCoordinate2D(latitude: self.lat, longitude: self.long))
                    self.mapView.animate(toZoom: 10)

                }
            }

        } else { // If we have address only
            if self.pharmacyAddress != "" {
                print("Find by address", pharmacyAddress)
                self.address.text = self.pharmacyAddress
                addressDidChanged()
            }
            
            print("Default")
            
            locationManager = CLLocationManager()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.distanceFilter = 50
            locationManager.startUpdatingLocation()
            locationManager.delegate = self
            
            
            placesClient = GMSPlacesClient.shared()
            mapView.isMyLocationEnabled = true
            
            listLikelyPlaces()
        }

       
    }
    

    func addressDidChanged() {
        CLGeocoder().geocodeAddressString(address.text!, completionHandler: { (placemarks, error) in
            if error != nil {
                print(error!)
                return
            } else {
                if placemarks!.count > 0 {
                    let placemark = placemarks?[0]
                    let location = placemark?.location
                    let coordinate = location?.coordinate
                    self.mapView.clear()
                    let marker = GMSMarker()
                    marker.position = coordinate!
                    marker.title = self.address.text!
                    //                marker.snippet = result.lines?[1]
                    marker.map = self.mapView
                    self.mapView.animate(toLocation: CLLocationCoordinate2D(latitude: coordinate!.latitude, longitude: coordinate!.longitude))
                    self.mapView.animate(toZoom: 10)
                } else {
                    print("No area of interest found.")
                }
            }
        })
        
        
    }
    
    
    // Populate the array with the list of likely places.
    func listLikelyPlaces() {
        // Clean up from previous sessions.
        likelyPlaces.removeAll()
        
        placesClient.currentPlace(callback: { (placeLikelihoods, error) -> Void in
            if let error = error {
                // TODO: Handle the error.
                print("Current Place error: \(error.localizedDescription)")
                return
            }
            
            // Get likely places and add to the list.
            if let likelihoodList = placeLikelihoods {
                for likelihood in likelihoodList.likelihoods {
                    let place = likelihood.place
                    self.likelyPlaces.append(place)
                }
            }
        })
    }
    
    
    @IBAction func ok(_ sender: UIButton) {
       _ = navigationController?.popViewController(animated: true)
    }

}

// Delegates to handle events for the location manager.
extension MapVC: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: zoomLevel)
        
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
        
        listLikelyPlaces()
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        mapView.clear()
        geocoder.reverseGeocodeCoordinate(coordinate) { (response, error) in
            guard error == nil else {
                return
            }
            
            if let result = response?.firstResult() {
                let marker = GMSMarker()
                marker.position = coordinate
                marker.title = result.lines?[0]
                marker.map = mapView
                let address = String(marker.title!)
                self.address.text = address
                self.delegate?.newAddress(text: address, longitude: String(coordinate.longitude), latitude: String(coordinate.latitude))
                
            }
        }
    }
    
    
}


