//
//  CreatingProfile.swift
//  Pharmacy
//
//  Created by 123 on 9/26/17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift


class CreatingProfile {
    
    static  func createProfile(params: Dictionary<String, String>, success: @escaping (_ response: Dictionary<String, String>) -> Void, failure: @escaping (_ error: String) -> Void) {
        let tokens = try! Realm().objects(Token.self)
        let accessToken = tokens[0].accessToken
        let header = [
            "LiviaApp-APIVersion" : "2.0",
            "LiviaApp-Token" : accessToken
            ]
        print(params)
        Alamofire.request("https://test.liviaapp.com/api/user", method: .patch, parameters: params, headers: header).validate().responseJSON { responseJSON in
            switch responseJSON.result {
            case .success:
                if let json = responseJSON.result.value {
                    print(json)

                    }
                success(params)
            case .failure(let error):
                let errorMessage = error.localizedDescription
                failure(errorMessage)
            }
        }

    }
}
