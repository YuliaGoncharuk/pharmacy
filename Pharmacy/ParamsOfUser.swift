//
//  ParamsOfUser.swift
//  Pharmacy
//
//  Created by 123 on 9/26/17.
//  Copyright © 2017 123. All rights reserved.
//


import Foundation
import RealmSwift


class ParametresOfUser : Object {
    dynamic var phoneIdNumber = ""
    dynamic var phoneCode = ""
    dynamic var phoneNumber = ""
    dynamic var userStatus = ""
    dynamic var authCode = ""
    dynamic var userRole = ""

    override static func primaryKey() -> String? {
        return "phoneIdNumber"
    }
}


