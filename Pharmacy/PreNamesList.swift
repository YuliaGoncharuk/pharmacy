//
//  preAdminList.swift
//  Pharmacy
//
//  Created by Mikhail Filimonov on 28.09.17.
//  Copyright © 2017 123. All rights reserved.
//


import UIKit
import RealmSwift

class PreNamesList: UIButton, UITableViewDelegate, UITableViewDataSource {
    
    
    var table = UITableView()
    var act = [() -> (Void)]()
    var superSuperView = UIView()
    let preNames = ["Dr.", "Mr", "Ms", "Mrs", "Prof"]
    var preName = ""
    
    func showItems() {
        fixLayout()
        
        if(table.alpha == 0) {
            self.layer.zPosition = 1
            UIView.animate(withDuration: 0.3
                , animations: {
                    self.table.alpha = 1
            })
            
        } else {
            UIView.animate(withDuration: 0.3
                , animations: {
                    self.table.alpha = 0;
                    self.layer.zPosition = 0
            })
        }
    }
    
    func initMenu() {
        
        self.setTitle(preNames[0], for: UIControlState())
        
        let phoneNumberLine = UIView(frame: CGRect(x: 0, y: self.frame.height, width: self.frame.size.width, height: 2))
        phoneNumberLine.backgroundColor = UIColor.lightGray
        self.addSubview(phoneNumberLine)
        
        var resp = self as UIResponder
        
        while !(resp.isKind(of: UIViewController.self) || (resp.isKind(of: UITableViewCell.self))) && resp.next != nil {
            resp = resp.next!
        }
        
        if let vc = resp as? UIViewController{
            superSuperView = vc.view
        } else if let vc = resp as? UITableViewCell{
            superSuperView = vc
        }
        
        table = UITableView()
        table.rowHeight = self.frame.height
        table.delegate = self
        table.dataSource = self
        table.isUserInteractionEnabled = true
        table.alpha = 0
        table.separatorColor = self.backgroundColor
        superSuperView.addSubview(table)
        superSuperView.backgroundColor = .red
        self.addTarget(self, action:#selector(DropMenuButton.showItems), for: .touchUpInside)

    }
    
    func initMenu(_ items: [String]) {
        
        var resp = self as UIResponder
        while !(resp.isKind(of: UIViewController.self) || (resp.isKind(of: UITableViewCell.self))) && resp.next != nil {
            resp = resp.next!
        }
        
        if let vc = resp as? UIViewController {
            superSuperView = vc.view
        }
        else if let vc = resp as? UITableViewCell {
            superSuperView = vc
        }
        
        table = UITableView()
        table.rowHeight = self.frame.height
        table.delegate = self
        table.dataSource = self
        table.isUserInteractionEnabled = true
        table.alpha = 0
        table.separatorColor = self.backgroundColor
        superSuperView.addSubview(table)
        self.addTarget(self, action:#selector(DropMenuButton.showItems), for: .touchUpInside)

    }
    
    
    func fixLayout() {
        let auxPoint2 = superSuperView.convert(self.frame.origin, from: self.superview)
        var tableFrameHeight = CGFloat()
        tableFrameHeight = self.frame.height * CGFloat(preNames.count)
        table.frame = CGRect(x: auxPoint2.x, y: auxPoint2.y + self.frame.height, width: self.frame.width, height:tableFrameHeight)
        table.rowHeight = self.frame.height
        table.reloadData()
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setNeedsDisplay()
        fixLayout()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return preNames.count
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        
        self.setTitle(preNames[indexPath.row], for: UIControlState())
        self.setTitle(preNames[indexPath.row], for: UIControlState.highlighted)
        self.setTitle(preNames[indexPath.row], for: UIControlState.selected)

        preName = preNames[indexPath.row]
        
        if self.act.count > 1 {
            self.act[indexPath.row]()
        }
        showItems()
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let preName = UILabel(frame: CGRect(x: 0, y: 5, width: self.frame.width, height: (self.frame.height)))
        preName.textAlignment = NSTextAlignment.left
        preName.text = preNames[indexPath.row]
        preName.font = self.titleLabel?.font
        preName.textColor = UIColor.darkGray
   
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.white
        
        let cell = UITableViewCell(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        cell.backgroundColor = .white
        cell.selectedBackgroundView = bgColorView
        cell.separatorInset = UIEdgeInsetsMake(0, self.frame.width, 0, self.frame.width)
        cell.addSubview(preName)

        
        return cell
    }
}


